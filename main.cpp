#include <cstdlib>
#include <iostream>

#include "pgmManager.h"

using namespace std;

int main(int argc, char** argv) {
    cout << "hello world" << endl;
    
    PGM *lena = PgmManager::load("lena.pgm");
    
    std::cout << (lena?"yes!":"no!") << std::endl;
    
    PgmManager::save("lenaCopy.pgm",lena);
    
    delete lena;
    
    return 0;
}