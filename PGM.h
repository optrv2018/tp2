/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PGM.h
 * Author: eleve
 *
 * Created on 22 novembre 2018, 10:53
 */

#ifndef PGM_H
#define PGM_H

#include <vector>

class PGM {
public:
    PGM(int width, int height, const std::vector<int> &data) : m_width(width),
                                                                m_height(height),
                                                                m_data(data) {};
    PGM(const PGM& orig);
    
    // Utilitaires
    PGM *histogramme();
    
    PGM *seuillage(unsigned int val);
    
    PGM *diff(PGM *other);
    
    PGM *resize(double ratio);
    
    // Accesseurs
    int getWidth()    {return m_width;  }
    int getHeight()   {return m_height; }
    int getData(int i){return m_data[i];}
    
private:
    std::vector<int> m_data;
    int m_width;
    int m_height;
};

#endif /* PGM_H */

