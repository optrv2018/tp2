/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   pgmManager.h
 * Author: eleve
 *
 * Created on 22 novembre 2018, 10:52
 */

#ifndef PGMMANAGER_H
#define PGMMANAGER_H

#include <string>
#include "PGM.h"

namespace PgmManager {
    PGM *load(std::string path);
    
    bool save(std::string path, PGM *image);
};

#endif /* PGMMANAGER_H */

