/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   pgmManager.cpp
 * Author: eleve
 * 
 * Created on 22 novembre 2018, 10:52
 */
#include "pgmManager.h"

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

PGM* PgmManager::load(std::string path) {
    ifstream file(path);
    
    if(!file.is_open())
        return nullptr;
    
    // lecture du fichier...
    std::string firstLine;
    std::string trash;
    
    file >> firstLine;
    
    file >> trash;
    
    int width, height;
    file >> width;
    file >> height;
    
    file >> trash;
    
    std::vector<int> data(width*height);
    for(int i=0;i<width*height;i++)
    {
        file >> data[i];
    }
    
    file.close();
    
    PGM *out = new PGM(width, height, data);
    
    return out;
}

bool PgmManager::save(std::string path, PGM* image) {
    ofstream file(path);
    
    if(!file.is_open())
        return false;
    
    int maxColorValue = 255;
    
    // écriture du fichier...
    file << "P2\n#\n" << image->getWidth() << " " << image->getHeight() << "\n255\n";

    for(int i=0;i<image->getHeight();++i) {
        for(int j=0;j<image->getWidth();++j)
            file << image->getData(i*image->getHeight()+j) << ((j < (image->getWidth()-1))?" ":"");
        file << '\n';
    }
    
    file.close();
    
    return true;
}
